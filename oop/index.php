<?php
    include("Animal.php");
    include("Frog.php");
    include("Ape.php");
    $sheep = new Animal("shaun");

    echo "Name: ",$sheep->name,"<br>"; // "shaun"
    echo "legs: ",$sheep->legs,"<br>"; // 4
    echo "cold blooded: ",$sheep->cold_blooded,"<br><br>"; // "no"

    $sungokong = new Ape("kera sakti");
    echo "Name: ",$sungokong->name,"<br>";
    echo "legs: ",$sungokong->legs,"<br>";
    echo "cold booded: ",$sungokong->cold_blooded,"<br>";
    echo "Yell: ";
    $sungokong->yell(); // "Auooo"
    echo "<br><br>";

    $kodok = new Frog("buduk");
    echo "Name: ",$kodok->name,"<br>";
    echo "legs: ",$kodok->legs,"<br>";
    echo "cold booded: ",$kodok->cold_blooded,"<br>";
    echo "Jump: ";
    $kodok->jump() ; // "hop hop"
?>